\documentclass[a4paper,12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{mathtools,amsthm,amssymb,stmaryrd,dashrule,proof-dashed,mathrsfs}
\usepackage{xspace}
\usepackage{xparse}
\usepackage{todonotes}
\usepackage{thmtools}
\usepackage{tikz}
\usepackage{tkz-graph}
\usepackage{msc}
\usepackage{xcolor}
\usepackage[breakable]{tcolorbox}
\usepackage[hidelinks]{hyperref}
\usepackage{cleveref}
\usepackage[toc]{glossaries}
\newglossary*{notations}{List of Notations}
\usepackage{makeidx}
\usepackage{nccmath}

%opening
\title{DM869 \- Advanced Topics In Concurrent Systems}
\author{
	Filip Široký
	\\
	\href{mailto:fiuir18@student.sdu.dk}{fiuir18@student.sdu.dk}
}
% definition
\newcommand*{\define}
	{\:\stackrel{\mathclap{\normalfont	
	\mbox{def}}}{=}\:}
\newcommand*{\definition}[2]
	{\pid{#1}\:\stackrel{\mathclap{\normalfont	
	\mbox{def}}}{=}\: #2}
% output port
\newcommand*{\outport}[1]
	{\ensuremath{\overline{#1}}}
% Process termination
\newcommand*{\nil}
	{\ensuremath{\boldsymbol{0}\xspace}}
% Restriction
\newcommand*{\rest}[1]{\:\backslash#1\:}
% Parallel processes
\newcommand*{\pp}{\ensuremath{\mathop{\boldsymbol{|}}}}
% Process name
\newcommand*{\m}[1]{\ensuremath{\mathsf{#1}}}
\newcommand*{\pid}[1]{\m{#1}}
% Choice
\newcommand*{\choice}[2]{\ensuremath{(#1+#2)}}
% Rename
\newcommand*{\rename}[3]{\ensuremath{\pid{#1}[#2/#3]}}
% Performing action
\newcommand*{\perf}[1]
	{\ensuremath{\:\stackrel{\mbox{#1}}{\to}\:}}
\newcommand*{\sync}
	{\ensuremath{\:\stackrel{\tau}{\to}\:}}
\newcommand*{\single}
	{\ensuremath{\:\stackrel{\alpha}{\to}\:}}
% Condition
\newcommand*{\cond}[3]{\m{if}\, #1 \, \m{then} \, #2 \, \m{else} \, #3}
\begin{document}

\pagenumbering{gobble}

\maketitle

\begin{abstract}
This document contains notes from DM869 by Filip Široký.
\end{abstract}

\newpage
\tableofcontents
\newpage


\pagenumbering{arabic}


\section{Introduction to Milner's CCS}
\subsection{CCS Process Construction}
\begin{itemize}
\item Process $\definition{PC}{input.\outport{output}.\pid{PC}}$ 
\begin{align*}
  \pid{PC} & \define input.\outport{output}.\pid{PC}\\
   &= input.\outport{output}.input.\outport{output}.\pid{PC} \\
   &= input.\outport{output}.input.\outport{output}.input.\outport{output}.\pid{PC} 
\end{align*}
\item Terminating process $\definition{PC}{first.second.\nil}$
\item Choice: $\definition{CM}{coin.\choice{\outport{coffee}.\pid{CM}}{\outport{tea}.\pid{CM}}}$
\item Parallel  processes: System $\definition{S}{\pid{FirstP} \pp SecondP}$ 
\item Restriction: $\definition{P}{(\pid{R}\mid\pid{Q})\space \rest{Op1} \space\rest{Op2}}$ 
\begin{itemize}
\item Hides ports Op1 and Op2 from being used at this point.
\item None of these ports can be used individually.
\item Ports can be used in communication together.
\end{itemize}
\item Relabelling: $\definition{VM}{coin.\outport{item}.\pid{VM}} $
\begin{itemize}
\item $\definition{CHM}{\rename{VM}{choc}{item}}$ becomes  $\definition{CHM}{coin.\outport{choc}.\pid{VM}} $
\item function f: $\pid{Act} \to \pid{Act}: f(\tau) = \tau, f(\outport{a}) = \outport{f(a)}$ for each label a
\end{itemize}
\item Sum of processes $P_1 + P_2 = \sum_{i\in\{1,2\}} P_i$
\end{itemize}

\subsection{Behaviour}

\begin{itemize}
\item $\pid{PC} \perf{in} \pid{PC_{1}} \perf{\outport{out}} \pid{PC}$
\begin{itemize}
\item $\definition{PC}{in.\outport{out}.\pid{PC}}$ performs action $in$ and evolves into $\pid{PC_2}$
\item $\definition{PC_1}{\outport{out}.\pid{PC}}$ performs action $\outport{out}$ and evolves into $\pid{PC}$
\end{itemize}
\item Handshake - synchronized transition:
$\pid{P}\pp\pid{Q} \sync \pid{P'}\pp\pid{Q'}$
\begin{itemize}
\item process P has input port $action$, process Q has output port $action$, they both perform action $action$
\end{itemize}
\end{itemize}
\newpage
\subsection{SOS Roles for CCS}
\setlength{\tabcolsep}{10pt}
\begin{tabular}{ccc}
$\infer[K \define P {\:[DEF]}]{K \single K'}{P \single P'}
$&$\infer[{[ACT]}]{\alpha.P\single P}{}$ 
\\\\
$\infer[j \in I {\:[SUM]}]{\sum_{i \in I}P_{i} \single P_{j}^{'}}{P_{j}\single P_{j}^{'}}$&
$\infer[{[LPAR]}]{P \pp Q \single P' \pp Q}{P\single P'}$
\\\\
$\infer[{[RPAR]}]{P \pp Q \single P \pp Q'}{Q\single Q'}$ & 
$\infer[{[COM]}]{P \pp Q \sync P' \pp Q'}{P\single P' \quad Q\single Q'}$ \\\\
$\infer[{[REL]}]{P[f]\single P'[f]}{P\single P'}$ & 
$\infer[\alpha, \outport{\alpha}\not\in L {[RES]}]{P \backslash L \single P' \backslash L}{P\single P'}$
\\\\
{\color{red}
$ \infer[n \geq 0 \: { [VAL] }]{a(x).P \perf{a(n)} \rename{P}{n}{x}}{} $}
 & {\color{red}
 $\infer[n \: { [EVAL] }]{\outport{a}\left(e\right).P \perf{\outport{a}(n)} P}{} $
}\\\\
$\infer[bexp is true{[COND_T]}]{\cond{bexp}{P}{Q}\single P'}{P\single P'}$ \\\\
$\infer[bexp is false{[COND_F]}]{\cond{bexp}{P}{Q}\single P'}{P\single P'}$ 
\end{tabular}
\subsection{Value Passing CCS}
{\color{red}Not in lecture yet}
\begin{itemize}
\item VAL rule describes receiving value n
\item EVAL rule describes result of evaluation of value e that is outputed
\item EVAL is bound to VAL as value output cannot be without its input
\item values can be used in conditionals - example of predecessor:
\begin{itemize}
\item $\definition{Pred}{in(x).\pid{Pred(x)}}$
\item $\definition{Pred(x)}{\cond{x = 0}{\outport{out}(0).\pid{Pred}}{\outport{out}(x-1).\pid{Pred}}}$
\end{itemize}
\end{itemize}
\end{document}

