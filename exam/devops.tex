\subsection{DevOps}\label{DevOps}
The software development is a time-consuming process especially with the increasing complexity of software and software systems.
From the need to speed up the process, DevOps was created. It began at the O'Reilly Velocity 2009 with a talk entitled "10+ Deploys Per Day: Dev and Ops Cooperation at Flickr." \footnote{\texttt{https://conferences.oreilly.com/velocity/velocity2009/public/schedule/detail/7641}}, when 10 deploys a day was unheard of \cite{DevOpsAdopt}. 

DevOps is a set of practices affecting team organization, the way of building and structure of the systems
the company makes. The team reorganization focuses on closings the gap between Dev and Ops teams, and increasing collaboration and communication between them\cite{DevOpsAdopt}. The practices aim to reduce the time between code being committed to the version control system and time of the change being deployed into the production environment with high-quality assurance\cite{DevOpsSW}.
The emphasis is on the mechanisms used to deliver the change from the developer to the production environment. It should have high availability, security, reliability, and others to ensure the goal of the approach is met. One method for quality assurance, as an example, is to create automated tests that are used right after the change is committed and stops any code not passing these tests. A different method is to promote the change to the production only partially and test in the environment directly\cite{DevOpsSW}.

The authors of DevOps: A Software Architect's Perspective \cite{DevOpsSW} identify 5 categories of DevOps practices. The first category is "Treat Ops as first-class citizens from the point of view of requirements".
These practices serve to provide high quality. Operations have a set of requirements for logging and monitoring as operators require a good way to troubleshoot issues. This is achieved by involving operations in the development of requirements\cite{DevOpsSW}.

"Make Dev more responsible for relevant incident handling." aims to shorten the time to fix an error after observing it\cite{DevOpsSW}. Organizations may achieve this by having the developer responsible for the monitoring at the beginning of the deployment and switch the responsibility onto Ops later on\cite{DevOpsSW}.

Have a uniform deployment process used by all to avoid errors and ensure higher quality. Allow tracking the deployments artifacts and parts involved in the deployment\cite{DevOpsSW}.

Use continuous deployment as a means to reduce the time between commit and deployment with emphasis on automated tests. Avoid human interaction in placing the new code into production as it is prone to errors\cite{DevOpsSW}.

Develop infrastructure as code with the same practices as application code. Avoid misconfiguration in deployment by creating deployment scripts. Use test-driven development, automated tests, and version control to avoid further mistakes\cite{DevOpsSW}.

In practice, we can identify two core capabilities essential to successful adoption of DevOps: Continuous Integration (CI) and Continuous Delivery (CD). Both sets of practices serve to reduce the cycle time, the time between requirement definition to deployment into production. They are supported by other extensions or supporting capabilities of DevOps \cite{DevOpsAdopt}. Together they form so-called DevOps pipeline or in other words DevOps cycle. After one iteration of the cycle, the DevOps team collects data from monitoring the deployed product and goes back to planing. At each step, there are tons of tools for implementing the cycle as the figure \ref{pipeline} shows.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{img/atlassian-marketplace.png}
\caption[DevOps Pipeline]{DevOps Pipeline \footnotemark}
\label{pipeline}
\end{figure}
\footnotetext{https://www.enterpriseirregulars.com/116202/race-pipeline-atlassian-aint-playin-introducing-devops
-marketplace/}
\subsubsection{Continuous Integration}\label{CIntegration}

As software and systems complexity increases, it is typical for a company to have multiple development teams working on separate components of one product. Together with external services, there is a need for integration between teams and even someone else's applications. The process of integration is not a simple task throughout the cycle of the application\cite{DevOpsAdopt}. 

That is where the continuous integration practice from Agile is utilized. Agile introduces the process of continuously integrating the systems during development to avoid incompatibilities and defects early on and save resources. The developers integrate their work regularly and run integration tests at least on a daily basis. 

The benefit is not only in the quality of the source code itself, but it can also help discover dependency issues between the components. Having this information before finishing the product can reduce the time needed to fix the issues.

Continuous integration has 10 practices identified by the author of Agile Manifesto Martin Fowler.\cite{DevOpsAdopt}
\newpage
\begin{description}
\item [Maintain a single-source repository] The emphasis while developing applications is to use version management tools to allow multi-user access, branching, merging and streaming. Especially for multi-platform development, it is important to have a common, cross-platform, single source repository. Without it, the different platforms won't be able to follow continuous integration practices.

\item [Automate the build] For continuous integration to be continuous, the build process has to be automated.
\item [Make your build self-testing] After automatically building the system, it also has to be validated. Both unit-tests for each component and integration tests should be executed after every build to achieve the goals of DevOps.
\item [Ensure that everyone commits to the mainline every day] To ensure as little issues with integration as possible developers have to integrate their work as often as possible. Otherwise, they could slow down the rest of the developers.
\item [Ensure that every commit builds the mainline on an integration machine] It is important to ensure that the automated integration and testing are actually used for every commit.
\item [Keep the build fast] Make sure to have fast builds as slow builds may disrupt the rest of continuous integration.
\item [Test in the clone of the production environment] This environment is also referred to as Staging environment. Testing in an environment different to the production one might not discover some of the issues and therefore it is important to make the testing environment as close to production as possible.
\item [Make it easy for anyone to get the latest executable] Anyone involved in the project should be able to validate what is being built.
\item [Make sure everyone can see what is happening] It is important for all collaborating teams to see each other's progress to help integrate.
\item [Automate deployment] The ultimate goal to get to continuous delivery by automating the process of deployment to testing, integration testing, staging and eventually even production environment.
\end{description}

\newpage
\subsubsection{Continuous Delivery}\label{CDelivery}

Continuous delivery extends the continuous integration concept. At the end of every continuous integration build, the build is delivered to the next stage in the continuous delivery lifecycle. The next stage is testing by Quality Assurance (QA) team. Next, operations team delivers the build to production environment. The goal is to maximize the speed of producing requested features\cite{DevOpsAdopt}. 

Continuous delivery has several environments. Dev Environment is used for unit testing and integration build. Functional and Performance tests are done in Test Environment. Stage environment runs acceptance tests before sending the build to production. Figure \ref{CD} shows the relationship of CD and CI to the environments.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{img/cdenv.png}
\caption[Continuous Delivery]{Continuous Delivery}
\label{CD}
\end{figure}

\subsubsection{Continuous Feedback}\label{CFeedback}

The important supporting practice is continuous feedback. It is beneficial to get information from each functional area of the delivery cycle for the next iteration of the life cycle. Without continuous feedback CD and CI wouldn't work as the developers wouldn't know issues and performance of the production environment. Especially useful can be continuous monitoring to ensure application performance in production and problems that tests might not be able to identify. For some systems, analyzing user behavior holds important information. The task for Ops is then to analyze and interpret the obtained data.